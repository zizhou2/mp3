const User = require('../models/user');
const Task = require('../models/task');

module.exports = (router) => {
  const taskRoute = router.route('/tasks');

  
  taskRoute.get(async (req, res) => {
    try {
      console.log('getting task');
      let query = Task.find({});
      // where
      if (req.query.where) {
        const where = JSON.parse(req.query.where);
        query.where(where);
      }
  
      // sort
      if (req.query.sort) {
        const sort = JSON.parse(req.query.sort);
        query.sort(sort);
      }
  
      // select
      if (req.query.select) {
        const select = JSON.parse(req.query.select);
        query.select(select);
      }
  
      // skip
      if (req.query.skip) {
        const skip = parseInt(req.query.skip, 10);
        query.skip(skip);
      }
  
      if (req.query.limit) {
        const limit = parseInt(req.query.limit, 10);
        query.limit(limit);
      }
  
      // count
      if (req.query.count === 'true') {
        const count = await query.countDocuments();
        return res.json({ message: "OK", data: count });
      }
      console.log(query);
      const tasks = await query.exec();
      res.json({ message: "OK", data: tasks });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });

  taskRoute.post(async (req, res) => {
    try {
      console.log('postting task');
      if (!req.body.name || !req.body.deadline) {
        return res.status(400).send({ message: 'Error', data: 'Name and Deadline are required.' });
      }
      let assignedUserName = 'unassigned';
      let assignedUser = "";
      if (req.body.assignedUser) {
        let user = await User.findById(req.body.assignedUser);
        if (user) {
          assignedUserName = user.name;
          assignedUser=req.body.assignedUser;
        }
      }
      const task = new Task({
        name: req.body.name,
        description: req.body.description,
        deadline: req.body.deadline,
        completed: req.body.completed || false,
        assignedUser,
        assignedUserName, 
      });
  
      await task.save();
      if (!task.completed && assignedUser) {
        await User.findByIdAndUpdate(assignedUser, { $push: { "pendingTasks": task._id } });
      }
      res.status(201).json({ message: 'OK', data: task });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });
  
  return router;
};
