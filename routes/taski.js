const User = require('../models/user');
const Task = require('../models/task');

module.exports = (router) => {
  const taskRoute = router.route('/tasks/:id');

  taskRoute.get(async (req, res) => {
    try {
      console.log('getting taski');
      const task = await Task.findById(req.params.id);
      if (!task) {
        return res.status(404).send({ message: 'Error', data: error.message });
      }
      res.status(200).send({ message: 'OK', data: task });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });

  taskRoute.delete(async (req, res) => {
    try {
      console.log('deleteing taski');
      const task = await Task.findByIdAndRemove(req.params.id);

      if (!task) {
        return res.status(404).send({ message: 'Error', data: "no task found" });
      }

      if (task.assignedUser) {
        const user = await User.findById(task.assignedUser);
        if (user) {
          user.pendingTasks = user.pendingTasks.filter(t => t.toString() !== task._id.toString());
          await user.save();
        }
      }

      res.status(200).send({ message: 'OK', data: "TaskDeleted" });
    } catch (err) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });

  taskRoute.put(async (req, res) => {
    try {
      console.log('putting taski');
      const task = await Task.findById(req.params.id);
      if (!task) {
        return res.status(404).send({ message: 'Error', data: error.message });
      }
  
      task.name = req.body.name;
      task.description = req.body.description;
      task.deadline = req.body.deadline;
      task.completed = req.body.completed;
      task.assignedUserName = 'unassigned';
      task.assignedUser = "";
      if (req.body.assignedUser) {
        let user = await User.findById(req.body.assignedUser);
        if (user) {
          task.assignedUserName = user.name;
          task.assignedUser=req.body.assignedUser;
        }
      }
      await task.save();
  
      if (req.body.assignedUser && req.body.assignedUser !== task.assignedUser) {
        if (task.assignedUser) {
          const oldUser = await User.findById(task.assignedUser);
          if (oldUser) {
            oldUser.pendingTasks = oldUser.pendingTasks.filter(t => t.toString() !== task._id.toString());
            await oldUser.save();
          }
        }
        

        const newUser = await User.findById(req.body.assignedUser);
        if (newUser) {
          newUser.pendingTasks.push(task._id);
          await newUser.save();
        }
      }
  
      res.json({ message: 'OK', data: task });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });

  return router;
};
