const User = require('../models/user');
const Task = require('../models/task');

module.exports = (router) => {
  const userRoute = router.route('/users/:id');

  userRoute.get(async (req, res) => {
    try {
      console.log('getting useri');
      const user = await User.findById(req.params.id);
      if (!user) {
        return res.status(404).send({ message: 'Error', data: error.message });
      }
      res.status(200).send({ message: 'OK', data: user });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });


  userRoute.delete(async (req, res) => {
    try {
      console.log('deleteing useri');
      const user = await User.findByIdAndRemove(req.params.id);

      if (!user) {
        console.log('no user deleteing');
        return res.status(404).send({ message: 'Error', data: "no user found" });
      }

      await Task.updateMany({ assignedUser: user._id }, {
        $set: { assignedUser: '', assignedUserName: 'unassigned' }
      });

      res.status(200).send({ message: 'OK', data: "UserDeleted" });
    } catch (err) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });


  userRoute.put(async (req, res) => {
    try {
      

      if (!req.body.name || !req.body.email) {
        return res.status(400).send({ message: 'Error', data: 'Name and email are required.' });
      }
  
      const existingUser = await User.findOne({ email: req.body.email, _id: { $ne: req.params.id } });
      if (existingUser) {
        return res.status(400).send({ message: 'Error', data: 'User already exit.' });
      }
  

      const updatedUser = await User.findByIdAndUpdate(req.params.id, {
        name,
        email,
        pendingTasks
      }, { new: true, runValidators: true });
  
      if (!updatedUser) {
        return res.status(400).send({ message: 'Error', data: 'Update User failed.' });
      }
  
      res.status(200).send({ message: 'OK', data: "User Updated" });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });

  return router;
}