const User = require('../models/user');
const Task = require('../models/task');

module.exports = (router) => {
  const userRoute = router.route('/users');


  userRoute.get(async (req, res) => {
    try {
      let query = User.find({});
  
      console.log('getting user');
      if (req.query.where) {
        const where = JSON.parse(req.query.where);
        query.where(where);
      }
  
      // sort
      if (req.query.sort) {
        const sort = JSON.parse(req.query.sort);
        query.sort(sort);
      }
  
      // select
      if (req.query.select) {
        const select = JSON.parse(req.query.select);
        query.select(select);
      }
  
      // skip
      if (req.query.skip) {
        const skip = parseInt(req.query.skip, 10);
        query.skip(skip);
      }
  
      if (req.query.limit) {
        const limit = parseInt(req.query.limit, 10);
        query.limit(limit);
      }
  

      if (req.query.count === 'true') {
        const count = await query.countDocuments();
        return res.json({ message: "OK", data: count });
      }
  
      const users = await query.exec();
      res.json({ message: "OK", data: users });
    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });


  userRoute.post(async (req, res) => {
    try {
      if (!req.body.name || !req.body.email) {
        return res.status(400).send({ message: 'Error', data: 'Name and email are required.' });
      }
      const existingUser = await User.findOne({ email: req.body.email });
      
      if (existingUser) {
        return res.status(400).send({ message: 'Error', data: 'User already exit.' });
      }
      console.log("pendingTasks are : "+req.body.pendingTasks)
      const user = new User({
        name: req.body.name,
        email: req.body.email,
        pendingTasks:req.body.pendingTasks,
      });
  
      await user.save();
      if (req.body.pendingTasks && req.body.pendingTasks.length > 0) {
        await Task.updateMany(
          { _id: { $in: req.body.pendingTasks } },
          { $set: { assignedUser: user._id } }
        );
      }
      res.status(201).json({ message: "OK", data: user });
      console.log("final data user are : "+user)

    } catch (error) {
      res.status(500).send({ message: 'Error', data: error.message });
    }
  });

  return router;
};


